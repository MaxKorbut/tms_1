dict1 = {'a': 1, 'b': 2, 'c': 3}
dict2 = {'c': 3, 'd': 4, 'e': 5}

dict_s = {}

dict_1_c = dict1.copy()
dict_2_c = dict2.copy()

dict_1_c.update(dict_2_c)

arr_k = []
for k in dict_1_c.keys():
    arr_k.append(k)

for k in arr_k:
    dict1_v = dict1.get(k)
    dict2_v = dict2.get(k)
    if dict1_v is None:
        v = [None, dict2_v]
    elif dict2_v is None:
        v = [dict1_v, None]
    else:
        v = [dict1_v, dict2_v]
    dict_s[k] = v
print(dict_s)
