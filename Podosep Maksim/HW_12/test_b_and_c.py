import unittest
from test_HW_12_unittest.bulls_and_cows import BullsAndCowsTest


class TestBaC(unittest.TestCase):

    def setUp(self):
        self.BaC_test = BullsAndCowsTest()

    def test_BaC_1(self):
        a = '1234'
        b = '1269'
        self.assertEqual(self.BaC_test.BaC(a, b), 'bull = 2, cow = 0')

    def test_BaC_2(self):
        a = '1234'
        b = '1234'
        self.assertEqual(self.BaC_test.BaC(a, b), 'you are win!!!')

    @unittest.expectedFailure
    def test_BaC_3(self):
        a = '1234'
        b = 'ZXCV'
        self.assertEqual(self.BaC_test.BaC(a, b), 'you are win!!!')

    @unittest.expectedFailure
    def test_BaC_4(self):
        a = '1234'
        b = '1235'
        self.assertEqual(self.BaC_test.BaC(a, b), 'bull = 2, cow = 0')
