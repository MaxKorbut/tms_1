from selenium.webdriver.common.by import By


class ContactUsPageLocator:

    LOCATOR_SELECTOR = (By.XPATH, '//div[@class="selector"]')
    LOCATOR_SUBJECT = (By.XPATH,
                       '//select[@id="id_contact"]//option[@value="2"]')
    LOCATOR_EMAIL = (By.XPATH, '//input[@id="email"]')
    LOCATOR_ORDER = (By.XPATH, '//input[@id="id_order"]')
    LOCATOR_MESSAGE = (By.XPATH, '//textarea[@id="message"]')
    LOCATOR_SUBMIT_BUTTON = (By.XPATH, '//button[@id="submitMessage"]')
    LOCATOR_SENT_MESSAGE = (By.XPATH, '//p[@class="alert alert-success"]')
