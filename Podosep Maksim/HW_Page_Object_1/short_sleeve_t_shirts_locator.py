from selenium.webdriver.common.by import By


class ShortSleeveTshirtsLocator:

    LOCATOR_ADD_TO_CART = (By.XPATH, '//p[@id="add_to_cart"]//span')
    LOCATOR_CART = (By.XPATH,
                    '//div[@class="shopping_cart"]'
                    '//a[@title="View my shopping cart"]')
    LOCATOR_CLOSE_WINDOW = (By.XPATH,
                            '//div[@class='
                            '"layer_cart_product col-xs-12 col-md-6"]'
                            '//span[@title="Close window"]')
