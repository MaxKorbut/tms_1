from pages.base_page import BasePage
from locator.short_sleeve_t_shirts_locator import ShortSleeveTshirtsLocator


class ShortSleeveTshirtsPage(BasePage):

    def product_add_to_cart(self):
        cart_button = self.find_element(
            ShortSleeveTshirtsLocator.LOCATOR_ADD_TO_CART)
        cart_button.click()

    def open_cart(self):
        button_open_cart = self.find_element(
            ShortSleeveTshirtsLocator.LOCATOR_CART)
        button_open_cart.click()

    def close_window(self):
        button_close_window = self.find_element(
            ShortSleeveTshirtsLocator.LOCATOR_CLOSE_WINDOW)
        button_close_window.click()
