from test_HW_12.string_pytest import string_test
import pytest


def test_1():
    assert string_test('aaavvvdgghh') == 'a3v3dg2h2',\
        'check the entered string'


def test_2():
    assert string_test('123569874') == 'enter the correct string',\
        'check the entered string'


@pytest.mark.xfail
def test_3():
    assert string_test('aaavvvd15gghh') == 'a3v3dg2h2',\
        'check the entered string'


@pytest.mark.xfail
def test_4():
    assert string_test('zxc') == 'z1x1c1',\
        'check the entered string'
