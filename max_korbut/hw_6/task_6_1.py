"""Кардинг"""


def check_number(number):
    """Проверяет пригодность числа, не является ли оно строкой и тд"""

    if isinstance(number, int):
        moon_alg(number)
    else:
        print("Вы ввели вообще не число")


def moon_alg(number):
    """Принимает число и проводит это число через алгоритм Луны"""

    number = str(number)
    number = number[::-1]
    if len(number) % 2 == 0:
        number_list = number[-1:0:-2]
        number_list_2 = number[::2]
    else:
        number_list = number[-2:0:-2]
        number_list_2 = number[::2]
    moon_list = []
    moon_list_2 = []
    for number in number_list:
        number = int(number)
        number = number * 2
        if number > 9:
            number -= 9
            moon_list.append(number)
        else:
            moon_list.append(number)
    for number in number_list_2:
        number = int(number)
        moon_list_2.append(number)
    sum_moon_list = 0
    sum_moon_list_2 = 0
    for i in moon_list:
        sum_moon_list += i
    for i in moon_list_2:
        sum_moon_list_2 += i
    sum_all = sum_moon_list + sum_moon_list_2
    validate(sum_all)


def validate(sum_all):
    """Проверяет итоговое число на кратность 10"""

    flag = False
    if sum_all % 10 == 0:
        flag = True
    else:
        flag = False
    print(flag)


check_number(4561261212345467)
check_number(4561261212345464)
check_number(5555555555554444)
check_number(76009244561)
check_number('qwertytreqwertqw')
check_number(5019717010103742)
check_number(378282246310005)
check_number(371449635398431)
check_number(378734493671000)
check_number(4012888888881881)
check_number(4561261212345467)
check_number(4561261212345467)
check_number(371449635398431)
