"""Вывести элементы, которые больше предыдущих"""


def more_then_previous(list_number: list):
    """Принемает список и возвращет список с числами, больше предыдущих"""

    new_number_list = []
    for number in list_number[1:]:
        index_number = list_number.index(number)
        if number > list_number[index_number - 1]:
            new_number_list.append(number)
    return new_number_list


print(more_then_previous([1, 5, 2, 4, 3]))
print(more_then_previous([1, 2, 3, 4, 5]))
