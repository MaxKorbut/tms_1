from selenium.webdriver.common.by import By


def test_count_dresses(browser):
    """Найти сколько объявлений в разделе Женская одежда, категория Платья"""

    browser.get("https://baraholka.onliner.by/")
    xpath = '//a[@href="./viewforum.php?f=255"]/../sup'
    count_dresses = browser.find_element(By.XPATH, xpath)
    print(f"Кол-во платьев - {count_dresses.text}")
