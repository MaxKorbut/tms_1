"""Написать декоратор, который будет замерять время декорируемой функции"""
from time import time, sleep


def decorator(func):

    def wrapper(text):
        start = time()
        func(text)
        sleep(5)
        end = time()
        print(f"Время работы функции - {end-start} секунд")
    return wrapper


@decorator
def print_text(text):
    print(text.title())


print_text('Hi my name is max')
