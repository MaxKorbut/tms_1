import xml.etree.ElementTree as Et
import re


def parse_xml(root, find_by, value):
    """Поиск совпадений в xml файле по нужным параметрам и значениям"""

    for child in root:
        for book in list(child):
            if find_by in book.tag and value in re.split(', | ', book.text):
                print(f"По параметру {find_by} со значением {value} "
                      f"была найдена книга c id {child.attrib['id']}")


if __name__ == "__main__":
    xml_data = Et.parse('library.xml')
    xml_root = xml_data.getroot()

    parse_xml(xml_root, 'price', '7')
    parse_xml(xml_root, 'title', 'Midnight')
    parse_xml(xml_root, 'author', 'Gambardella')
    parse_xml(xml_root, 'description', 'bad')
