"""Найти самую частую букву"""
from collections import Counter

str = "One 111"
"""
Переведем строку в нижний регистр и при наличии нескольких слов в
одной строке исключим пробелы
"""
str_1 = str.lower()
str_1 = str_1.split()
str_1 = ''.join(str_1)
"""Исключим цифры и иные знаки"""
exceptions = [',', '.', '!', '-', '?',
              '0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
clear_str = []
for i in str_1:
    if i in exceptions:
        continue
    else:
        clear_str.append(i)
"""Узнаем количество каждого элемента"""
str_1_count = Counter(clear_str)
"""Получим список с количеством каждого элемента"""
values = []
for value in str_1_count.values():
    values.append(value)
"""
Список с кол-вом каждого элемента от большего к меньшему
и максимальное кол-во повторений
"""
values.sort(reverse=True)
max_number = values[0]
"""Получим список с самыми встречающимися элементами"""
str_1_number = []
for element, count in str_1_count.items():
    if count == max_number:
        str_1_number.append(element)
    else:
        continue
"""Получим список в порядке по алфавиту"""
str_1_number.sort()
need_number = str_1_number[0]
print(f"Самый частый элемент в строке '{str}' - {need_number}")
