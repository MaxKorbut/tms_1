"""Перевести строку в массив"""
str_1 = "Robin Singh"
str_2 = "I love arrays they are my favorite"

list_1 = str_1.split()
list_2 = str_2.split()

print(f"Строка '{str_1}' стала массивом {list_1}",
      f"\nСтрока '{str_2}' стала массивом {list_2}")
