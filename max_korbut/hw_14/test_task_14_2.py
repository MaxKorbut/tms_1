from selenium.webdriver.support.ui import Select


def test_register(browser):
    browser.get('http://demo.guru99.com/test/newtours/register.php')
    first_name = 'Maks'
    last_name = 'Korbut'
    phone = '+375 29 111-11-11'
    email = 'maks@gmail.com'
    address = 'Uborevicha street'
    city = 'Minsk'
    state = 'Minsk region'
    postal_code = '220077'
    country = 'BELARUS'
    user_name = 'Maks'
    passwd = '1111'

    first_name_field = browser.find_element_by_name('firstName')
    first_name_field.send_keys(first_name)

    last_name_field = browser.find_element_by_name('lastName')
    last_name_field.send_keys(last_name)

    phone_field = browser.find_element_by_name('phone')
    phone_field.send_keys(phone)

    email_field = browser.find_element_by_id('userName')
    email_field.send_keys(email)

    address_field = browser.find_element_by_name('address1')
    address_field.send_keys(address)

    city_field = browser.find_element_by_name('city')
    city_field.send_keys(city)

    state_field = browser.find_element_by_name('state')
    state_field.send_keys(state)

    postal_code_field = browser.find_element_by_name('postalCode')
    postal_code_field.send_keys(postal_code)

    country_field = Select(browser.find_element_by_name('country'))
    country_field.select_by_visible_text(country)

    user_name_field = browser.find_element_by_name('email')
    user_name_field.send_keys(user_name)

    passwd_field = browser.find_element_by_name('password')
    passwd_field.send_keys(passwd)

    passwd_confirm_field = browser.find_element_by_name('confirmPassword')
    passwd_confirm_field.send_keys(passwd)

    submit_button = browser.find_element_by_name('submit')
    submit_button.click()

    xpath_name = "//font/b[contains(text(), 'Dear')]"
    full_name_check = browser.find_element_by_xpath(xpath_name)
    assert full_name_check.text == f"Dear {user_name} {last_name},"

    xpath_user_name = "//font/b[contains(text(), 'Note:')]"
    user_name_check = browser.find_element_by_xpath(xpath_user_name)
    assert user_name_check.text == f"Note: Your user name is {user_name}."
