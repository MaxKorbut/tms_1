from pages.base_page import BasePage
from locators.contact_us_locator import ContactUsPageLocator
from selenium.webdriver.support.ui import Select


class ContactUsPage(BasePage):

    def open_contact_us_page(self):
        contact_us_button = self.find_element(
            ContactUsPageLocator.LOCATOR_CONTACT_US_BUTTON)
        contact_us_button.click()

    def should_be_contact_us_page(self):
        contact_us_text = self.find_element(
            ContactUsPageLocator.LOCATOR_CONTACT_US_TEXT)
        assert contact_us_text.text == "CUSTOMER SERVICE - CONTACT US"

    def fill_in_form(self, sub_heading, email, order_reference, message):
        subject_heading_field = self.find_element(
            ContactUsPageLocator.LOCATOR_SUBJECT_HEADING_FIELD)
        select_field = Select(subject_heading_field)
        select_field.select_by_visible_text(sub_heading)

        email_address_field = self.find_element(
            ContactUsPageLocator.LOCATOR_EMAIL_ADDRESS_FIELD)
        email_address_field.send_keys(email)

        order_reference_field = self.find_element(
            ContactUsPageLocator.LOCATOR_ORDER_REFERENCE)
        order_reference_field.send_keys(order_reference)

        message_field = self.find_element(
            ContactUsPageLocator.LOCATOR_MESSAGE_FIELD)
        message_field.send_keys(message)

        send_button = self.find_element(
            ContactUsPageLocator.LOCATOR_SEND_BUTTON)
        send_button.click()

    def get_success_fill_in_message(self):
        success_message = self.find_element(
            ContactUsPageLocator.LOCATOR_SUCCESS_MESSAGE)
        return success_message
