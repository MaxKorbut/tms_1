from selenium.webdriver.common.by import By


class ContactUsPageLocator:

    LOCATOR_CONTACT_US_BUTTON = (By.ID, 'contact-link')
    LOCATOR_CONTACT_US_TEXT = (By.CLASS_NAME, 'page-heading')
    LOCATOR_SUBJECT_HEADING_FIELD = (By.XPATH,
                                     '//select[@class="form-control"]')
    LOCATOR_EMAIL_ADDRESS_FIELD = (By.XPATH, '//input[@id="email"]')
    LOCATOR_ORDER_REFERENCE = (By.ID, 'id_order')
    LOCATOR_MESSAGE_FIELD = (By.XPATH, '//textarea[@class="form-control"]')
    LOCATOR_SEND_BUTTON = (By.ID, 'submitMessage')
    LOCATOR_SUCCESS_MESSAGE = (By.CLASS_NAME, 'alert-success')
