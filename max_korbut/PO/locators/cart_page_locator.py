from selenium.webdriver.common.by import By


class CartPageLocator:

    LOCATOR_CART_BUTTON = (By.XPATH, '//div[@class="shopping_cart"]//a')
    LOCATOR_CART_PAGE_TEXT = (By.CLASS_NAME, 'navigation_page')
    LOCATOR_STATUS_CART_TEXT = (By.CLASS_NAME, 'alert-warning')
    LOCATOR_PRODUCT_NAME_TEXT = (By.XPATH, '//tbody//p[@class="product-name"]')
