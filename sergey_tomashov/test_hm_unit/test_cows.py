from cows import cows
import unittest
import random


class TestCows(unittest.TestCase):

    def setUp(self):
        self.number = ''.join(random.sample('1234567890', 4))

    def test_correct(self):
        self.assertEqual(cows(self.number, self.number),
                         f'Congrats! You {self.number} same as {self.number}')

    def test_cows(self):
        self.assertEqual(cows(self.number, self.number[::-1]),
                         'Cows :4, Bulls: 0')

    def test_bulls(self):
        self.assertEqual(cows('1234', '0231'), 'Cows :1, Bulls: 2')

    def test_less_then_4(self):
        self.assertEqual(cows(self.number, self.number[1:2]),
                         f'{self.number[1:2]} input '
                         'should be 4 non repeatable digits')

    def test_not_digits(self):
        self.assertEqual(cows(self.number, 'abcd'),
                         'abcd input should be 4 '
                         'non repeatable digits')

    def test_repeat_digits(self):
        self.assertEqual(cows(self.number,
                              self.number[0:2] + self.number[0:2]),
                         'Repeatable digits in input')

    def test_spaces(self):
        self.assertEqual(cows(self.number, '1 2 3 4'),
                         '1 2 3 4 input should be 4 non repeatable digits')

    @unittest.expectedFailure
    def test_empty_input(self):
        self.assertEqual(cows(self.number),
                         f'Congrats! You {self.number} same as {self.number}')
