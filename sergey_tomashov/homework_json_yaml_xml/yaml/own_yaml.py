import yaml

to_yaml = {'first_name': 'Sergey', 'access': 'admin'}

with open('own_file.yaml', 'w') as f:
    yaml.dump(to_yaml, f)
