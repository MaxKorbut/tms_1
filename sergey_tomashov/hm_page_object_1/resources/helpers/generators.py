import random
from string import ascii_lowercase


def generate_email():
    email = ''.join(random.choice(ascii_lowercase)
                    for i in range(0, 10)) + '@gmail.com'
    return email


def generate_order():
    order = ''.join(random.choice('1234567890')
                    for i in range(0, 10))
    return order


def generate_message():
    mes = ''.join(random.choice(ascii_lowercase)
                  for i in range(0, 20))
    return mes
