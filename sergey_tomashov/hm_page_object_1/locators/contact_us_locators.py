from selenium.webdriver.common.by import By


class ContuctUsLocators:
    LOCATOR_SUBJECT_HEADING = (By.ID, "id_contact")
    LOCATOR_EMAIL = (By.ID, "email")
    LOCATOR_ORDER_REF = (By.ID, "id_order")
    LOCATOR_MESSAGE = (By.ID, "message")
    LOCATOR_SUBMIT_FORM = (By.ID, "submitMessage")
    LOCATOR_SUCCESS = (By.XPATH, "//p[@class='alert alert-success']")
