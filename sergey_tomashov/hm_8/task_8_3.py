from string import digits


def validate_choice(value):
    if value not in '123456':
        print('Некорректный ввод\n')
        return False
    else:
        return True


def validate_value(value1, value2):
    for i in value1 + value2:
        if i not in digits:
            print('Некорректный ввод\n')
            return False
    return True


def summ(a, b):
    return a + b


def mult(a, b):
    return a * b


def dif(a, b):
    return a - b


def div(a, b):
    return a / b


def complex(s):
    s = s.split()
    while len(s) != 1:
        for i in s:
            if i in ['/']:
                operator_index = s.index(i)
                first_digit = float(s[operator_index - 1])
                second_digit = float(s[operator_index + 1])
                result = div(first_digit, second_digit)
                s[operator_index - 1] = float(result)
                del s[operator_index + 1]
                del s[operator_index]

        for i in s:
            if i in ['*']:
                operator_index = s.index(i)
                first_digit = float(s[operator_index - 1])
                second_digit = float(s[operator_index + 1])
                result = mult(first_digit, second_digit)
                s[operator_index - 1] = float(result)
                del s[operator_index + 1]
                del s[operator_index]

        for i in s:
            if i in ['-']:
                operator_index = s.index(i)
                first_digit = float(s[operator_index - 1])
                second_digit = float(s[operator_index + 1])
                result = dif(first_digit, second_digit)
                s[operator_index - 1] = float(result)
                del s[operator_index + 1]
                del s[operator_index]

        for i in s:
            if i in ['+']:
                operator_index = s.index(i)
                first_digit = float(s[operator_index - 1])
                second_digit = float(s[operator_index + 1])
                result = summ(first_digit, second_digit)
                s[operator_index - 1] = float(result)
                del s[operator_index + 1]
                del s[operator_index]
        return s


while True:
    choice = input('1. Сложение\n'
                   '2. Вычитание\n'
                   '3. Умножение\n'
                   '4. Деление\n'
                   '5. Умный калькулятор\n'
                   '6. Выход')

    if validate_choice(choice):
        if choice == '6':
            break
        if choice == '5':
            expression = input('Введите выражение для подсчета '
                               '(все символы через пробел):')
            result = complex(expression)
            print('Результат:' + str(result))
            continue

        a = input('Enter int')
        b = input('Enter int')
        if validate_value(a, b):

            a = int(a)
            b = int(b)
            if choice == '1':
                print(summ(a, b))
            if choice == '2':
                print(dif(a, b))
            if choice == '3':
                print(mult(a, b))
            if choice == '4':
                div_r = div(a, b)
                c = str(div_r).split('.')
                print(f'Частное: {c[0]}\n'
                      f'Остаток: {c[1]}')
