def typed(typ):
    def decorator(func):
        def wrapper(*args):
            ls = []
            if typ == 'str':
                for i in args:
                    ls.append(str(i))
                tup = tuple(ls)
                return func(*tup)
            if typ == 'int':
                for i in args:
                    ls.append(int(i))
                tup = tuple(ls)
                return func(*tup)

        return wrapper

    return decorator


@typed(typ='str')
def add(a, b, c):
    return a + b + c


@typed(typ='int')
def add1(a, b):
    return a + b


print(add(1, '3', 5))
print(add1(1, '3'))
