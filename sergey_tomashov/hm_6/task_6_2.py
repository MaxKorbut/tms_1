from collections import Counter


def string_counter(string):
    string = Counter(string)
    count_string = ''
    for k, v in string.items():
        count_string += k
        if v > 1:
            count_string += str(v)
    return count_string


print(string_counter('aaabbbccc'))
