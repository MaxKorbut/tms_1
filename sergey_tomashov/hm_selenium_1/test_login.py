import pytest
import random
import string


class TestLogin():

    @pytest.fixture()
    def register_user(self, browser):
        browser.get('http://thedemosite.co.uk/addauser.php')
        self.username = ''.join(random.sample(string.ascii_lowercase, 4))
        self.passw = ''.join(random.sample(string.ascii_lowercase, 6))
        fusername = browser.find_element_by_name('username')
        fpassword = browser.find_element_by_name('password')
        fsubmit = browser.find_element_by_name('FormsButton2')
        fusername.send_keys(self.username)
        fpassword.send_keys(self.passw)
        fsubmit.click()
        return self.username, self.passw

    def test_login(self, browser, register_user):
        browser.get('http://thedemosite.co.uk/login.php')
        username = browser.find_element_by_name('username')
        password = browser.find_element_by_name('password')
        submit = browser.find_element_by_name('FormsButton2')
        username.send_keys(self.username)
        password.send_keys(self.passw)
        submit.click()
        success = browser.find_element_by_xpath("//center/b")
        assert success.text == "**Successful Login**"
