import pytest
import random
import string
from selenium.webdriver.support.ui import Select


class TestLogin():

    @pytest.fixture()
    def prepare_user(self, browser):
        self.first_name = ''.join(random.sample(string.ascii_lowercase, 6))
        self.last_name = ''.join(random.sample(string.ascii_lowercase, 6))
        self.phone = ''.join(random.sample('1234567890', 8))
        self.email = self.first_name + "@mail.com"
        self.address = 'any adress 16'
        self.city = 'Minsk'
        self.state = 'any state'
        self.postal = self.phone = ''.join(random.sample('1234567890', 6))
        self.username = self.first_name
        self.passw = ''.join(random.sample('1234567890', 8))
        self.select_index = ''.join(random.sample('1234567890', 2))

    def test_login(self, browser, prepare_user):
        browser.get('http://demo.guru99.com/test/newtours/register.php')
        browser.find_element_by_name('firstName').send_keys(self.first_name)
        browser.find_element_by_name('lastName').send_keys(self.last_name)
        browser.find_element_by_name('phone').send_keys(self.phone)
        browser.find_element_by_id('userName').send_keys(self.email)
        browser.find_element_by_name('address1').send_keys(self.address)
        browser.find_element_by_name('city').send_keys(self.city)
        browser.find_element_by_name('state').send_keys(self.state)
        browser.find_element_by_name('postalCode').send_keys(self.postal)
        select = Select(browser.find_element_by_name('country'))
        select.select_by_index(self.select_index)
        browser.find_element_by_id('email').send_keys(self.email)
        browser.find_element_by_name('password').send_keys(self.passw)
        browser.find_element_by_name('confirmPassword').send_keys(self.passw)
        browser.find_element_by_name('submit').click()
        text1 = browser.find_element_by_xpath("//font/b[contains(text(), "
                                              "'Dear')]").text
        text2 = browser.find_element_by_xpath("//font/b[contains(text(), "
                                              "'Note:')]").text
        assert f'Dear {self.first_name} {self.last_name}' in text1
        assert self.email in text2
