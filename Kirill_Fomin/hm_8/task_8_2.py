number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
                14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
                17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}


def print_kwargs(func):

    def wrapper(vel):
        new = [j for s in vel.split()
               for i, j in number_names.items() if int(s) == i]

        kon = [i for n in sorted(new)
               for i, j in number_names.items() if n == j]

        return func(kon)

    return wrapper


@print_kwargs
def add_fit(vel):
    print(vel)


add_fit('1 2 3')
add_fit('3 5 6 8 13 19 7 0 4')
add_fit('1 2 8 5 9 3 7')
add_fit('3 5 6 1')
