from time import time, sleep


def caching(timeout):
    def decorator(func):
        cache = {}

        def inner(x):
            start = time()
            if x not in cache.keys():
                cache[func(x)] = start
                print(f'New meaning in cache {cache}')
            elif x in cache.keys() and start - cache[x] < timeout:
                print(f'Repeat value in cache {cache}')
            else:
                print(f'Old value in cache {cache}')
        return inner
    return decorator


@caching(timeout=3)
def func(x):
    return x


func(2)
sleep(1)
func(2)
sleep(1)
func(4)
sleep(1)
func(5)
