from time import time, sleep


def decorator(func):
    def wrapper(x):
        start = time()
        func(x)
        delta_time = time() - start
        print(f"Function running time {round(delta_time, 5)} seconds")
        return delta_time
    return wrapper


@decorator
def tre(x):
    print("Func started")
    sleep(3)
    res = x ** 5
    print(f'Functional result is {res}')


print(tre(5))
print(tre(4))
print(tre(3))
