# Validate


def validate(cart):
    assert int(cart), "Ошибка при вводе!"

    sew = str(cart)
    s = 0
    for i in sew[::2]:
        n = int(i)
        if 2 * n > 9:
            s += 2 * n - 9
        else:
            s += 2 * n
    for j in sew[1::2]:
        m = int(j)
        s += m
    if s % 2 == 0:
        print(True)
    else:
        print(False)


validate(4561261212345464)
validate(4561261212345467)
validate(5555555555554444)
validate(76009244561)
validate(5019717010103742)
validate(378282246310005)
validate(371449635398431)
validate(378734493671000)
validate(4012888888881881)
validate(4561261212345467)
validate(4561261212345467)
validate(371449635398431)
validate(37828224631000)
validate(5610591081018250)
validate(30569309025904)
validate(4111111111111111)
validate(4222222222222)
validate(4012888888881881)
validate(6331101999990016)
