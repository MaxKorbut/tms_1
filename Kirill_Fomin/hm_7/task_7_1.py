
def item_larger_previous_item(list_k: list):

    dex = []
    for i in range(1, len(list_k)):
        n = list_k[i]
        if n > list_k[i - 1]:
            dex += [n]
    return dex


print(item_larger_previous_item([1, 5, 2, 4, 3]))

print(item_larger_previous_item([1, 2, 3, 4, 5]))

print(item_larger_previous_item([2, 8, 9, 7, 6, 10, 11]))
