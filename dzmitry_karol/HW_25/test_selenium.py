from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def test_checkbox(browser):
    browser.get("http://the-internet.herokuapp.com/dynamic_controls")
    checkbox = browser.find_elements(By.XPATH, '//input[@type="checkbox"]')
    assert checkbox, 'the checkbox is not exist on the page'
    remove_btn = browser.find_element(By.XPATH, '//button[text()="Remove"]')
    remove_btn.click()
    msg = '//p[@id="message"]'
    WebDriverWait(browser, 10).until(
        EC.visibility_of_element_located((By.XPATH, msg)))
    checkbox = browser.find_elements(By.XPATH, '//input[@type="checkbox"]')
    assert not checkbox, "the checkbox is exist on the page"
    input_str_path = '//form[@id="input-example"]/input'
    input_str = browser.find_element(By.XPATH, input_str_path)
    assert not input_str.is_enabled(), 'the field is enabled'
    enable_btn = browser.find_element(By.XPATH, '//button[text()="Enable"]')
    enable_btn.click()
    msg = '//p[@id="message"]'
    WebDriverWait(browser, 10).until(
        EC.visibility_of_element_located((By.XPATH, msg)))
    assert input_str.is_enabled(), 'the field is disabled'


def test_frame(browser):
    browser.get("http://the-internet.herokuapp.com/frames")
    frame_link = browser.find_element(By.XPATH, '//a[text()="iFrame"]')
    frame_link.click()
    frame_path = '//iFrame[@id="mce_0_ifr"]'
    browser.switch_to.frame(browser.find_element(By.XPATH, frame_path))
    frame_text = browser.find_element(By.XPATH, '//p').text
    assert 'Your content goes here.' == frame_text
