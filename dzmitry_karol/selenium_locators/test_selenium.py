from selenium.webdriver.common.by import By


def test_button(browser):
    browser.get("https://ultimateqa.com/complicated-page/")
    xpath = '//a[@class="et_pb_button et_pb_button_4 et_pb_bg_layout_light"]'
    button_first = browser.find_element(By.XPATH, xpath)
    button_first.click()
    button_by_css = '.et_pb_button.et_pb_button_4'
    button_second = browser.find_element(By.CSS_SELECTOR, button_by_css)
    button_second.click()
    button_by_class = 'et_pb_button_4'
    button_third = browser.find_element(By.CLASS_NAME, button_by_class)
    button_third.click()


def test_form_submit(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")
    name_field = browser.find_element(By.ID, "et_pb_contact_name_0")
    name_field.send_keys("dzmitry")
    msg_field = browser.find_element(By.ID, "et_pb_contact_message_0")
    msg_field.send_keys('Hello world!')
    button_path = '//*[@id="et_pb_contact_form_0"]/div[2]/form/div/button'
    submit_button = browser.find_element(By.XPATH, button_path)
    submit_button.click()
    result_path = '//*[@id="et_pb_contact_form_0"]/div/p'
    result = browser.find_element(By.XPATH, result_path).text
    assert "Form filled out successfully" == result


def test_form_message_error(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")
    name_field = browser.find_element(By.ID, "et_pb_contact_name_0")
    name_field.send_keys("dzmitry")
    button_path = '//*[@id="et_pb_contact_form_0"]/div[2]/form/div/button'
    submit_button = browser.find_element(By.XPATH, button_path)
    submit_button.click()
    error_field = '//*[@id="et_pb_contact_form_0"]/div[1]/ul[1]/li'
    result = browser.find_element(By.XPATH, error_field).text
    assert "Message" == result


def test_form_name_error(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")
    msg_field = browser.find_element(By.ID, "et_pb_contact_message_0")
    msg_field.send_keys('Hello world!')
    button_path = '//*[@id="et_pb_contact_form_0"]/div[2]/form/div/button'
    submit_button = browser.find_element(By.XPATH, button_path)
    submit_button.click()
    error_field = '//*[@id="et_pb_contact_form_0"]/div[1]/ul[1]/li'
    result = browser.find_element(By.XPATH, error_field).text
    assert "Name" == result
