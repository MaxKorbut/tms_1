list_1 = [1, 2, 3, 4, 5, 6]
list_2 = [1, 2, 3, 4, 5, 6, 7, 8]


def reshape(arr: list, y: int, x: int):
    array = [[] for i in range(y)]
    gen = (el for el in arr)
    for i in array:
        if len(i) < x:
            for j in gen:
                i.append(j)
                if len(i) >= x:
                    break
    return array


print(reshape(list_1, 2, 3))
print(reshape(list_2, 4, 2))
