print('1 - Сложение\n2 - Вычитание\n2 - Умножение\n4 - Деление')
op = int(input('Введите номер пункта меню:'))
a = int(input('Введите первое число:'))
b = int(input('Введите второе число:'))


def calc(operation, first, second):
    res = 0
    if operation == 1:
        res = first + second
    elif operation == 2:
        res = first - second
    elif operation == 3:
        res = first * second
    elif operation == 4:
        if second == 0:
            print('На ноль делить нельзя.')
        else:
            res = str(first / second).split('.')
            res = f'частное - {res[0]}, остаток - {res[1]}'
    print(f'Результат: {res}')


calc(op, a, b)
