class Investment:
    def __init__(self, client, amount, term):
        self.user = client
        self.amount = amount
        self.term = term

    def invest(self):
        print(f'{self.user}, You made invested: {self.amount}'
              f' for {self.term} year.')


class Bank:
    def __init__(self, amount, term):
        self.amount = amount
        self.term = term

    def deposit(self):
        date = self.term * 12
        for i in range(date):
            res = ((self.amount * (1 + 10 / 100) - self.amount) / 12)
            self.amount = self.amount + res
        print(f'Revenue: {round(self.amount - n, 2)} $')
        print(f'Summ: {round(self.amount, 2)} $')


user = 'Dzmitry'
n = 2500
r = 1

operation = Investment(user, n, r)
operation.invest()
result = Bank(n, r)
result.deposit()
