class Book:
    def __init__(self, title, author, page_count, isbn, status=False,
                 student=None):
        self.title = title
        self.author = author
        self.page_count = page_count
        self.isbn = isbn
        self.status = status
        self.student = student


class User:
    def __init__(self, user_name, books=None):
        if books is None:
            books = []
        self.user_name = user_name
        self.books = books

    def take_book(self, book):
        if not book.status:
            book.status = True
            self.books.append(book.isbn)
            print(f'{self.user_name}, You take book: {book.title}')
        else:
            print(f'The book {book.title} reserved by another student')

    def return_book(self, book):
        if book.isbn in self.books:
            book.status = False
            self.books.remove(book.isbn)
        else:
            print('You do not have this book')

    def book_reservation(self, book):
        if not book.reserved:
            book.student = self.user_name
            book.status = True
            print(f'The {book.title} book is reserve by {book.student}')
        elif book.isbn in self.books:
            print(f'You received book {book.title} earlier')
        else:
            print(f'{book.title} is reserved')


book1 = Book('Война и Мир', 'Толстой', 5500, 18736)
book2 = Book('Му-Му', 'Тургенев', 145, 93249)
user1 = User('Alex')
user2 = User('Dzmitry')

user1.take_book(book1)
user2.take_book(book1)
user2.return_book(book1)
user1.return_book(book1)
user2.take_book(book1)
