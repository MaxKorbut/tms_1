"""Откройте страницу: https://ultimateqa.com/complicated-page/
Нажмите на 2-ю сверху кнопку во втором столбце используя:
XPATH
CSS selector
class name"""


def test_button(browser):
    browser.get("https://ultimateqa.com/complicated-page/")

    by_xpath = browser.find_element_by_xpath(
        "//a[@class='et_pb_button et_pb_button_4 et_pb_bg_layout_light']")
    assert by_xpath.text == 'Button'

    by_css = browser.find_element_by_css_selector(
        "[class='et_pb_button et_pb_button_4 et_pb_bg_layout_light']")
    assert by_css.text == 'Button'

    by_class = browser.find_element_by_class_name('et_pb_button_4')
    assert by_class.text == 'Button'
