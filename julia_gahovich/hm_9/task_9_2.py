"""Создайте класс инвестиция. Который содержит необходимые поля и методы,
например сумма инвестиция и его срок. Пользователь делает инвестиция в размере
N рублей сроком на Rлет под 10% годовых (инвестиция с возможностью ежемесячной
капитализации - это означает, что проценты прибавляются к сумме инвестиции
ежемесячно). Написать класс Bank, метод deposit принимает аргументы N и R, и
возвращает сумму, которая будет на счету пользователя.
"""


class Investment:

    def __init__(self, amount, term_days):
        self.amount = amount
        self.term_days = term_days


class Deposit:
    month_days = 30
    year_days = 365

    def __init__(self, rate):
        self.rate = rate

    def calculate(self, amount, rate, term_days):
        return round(amount * (
                     1 + rate * self.month_days / self.year_days / 100
                     ) ** (term_days / self.month_days), 2)

    def deposit(self, investment: Investment):
        return self.calculate(investment.amount, self.rate,
                              investment.term_days)


julia_investment = Investment(50000, 90)
alfa_bank = Deposit(10.5)
techno_bank = Deposit(10)
print(f'Сумма в Альфа банке: {alfa_bank.deposit(julia_investment)}' + 'р')
print(f'Сумма в Техно банке: {techno_bank.deposit(julia_investment)}' + 'р')
