"""В классическом варианте игра рассчитана на двух игроков. Каждый из игроков
задумывает и записывает тайное 4-значное число с неповторяющимися цифрами.
Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое
противнику. Противник сообщает в ответ, сколько цифр угадано без совпадения с
их позициями в тайном числе (то есть количество коров) и сколько угадано
вплоть до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой, пока не
отгадает всю последовательность. Ваша задача реализовать программу,
против которой можно сыграть в "Быки и коровы"
"""
import random

cows_sms = {0: 'Ноль коров,', 1: 'Одна корова,', 2: 'Две коровы,',
            3: 'Три коровы,', 4: 'Четыре коровы,'}
bulls_sms = {0: 'ноль быков', 1: 'один бык', 2: 'два быка',
             3: 'три быка', 4: 'четыре быка'}


def computer_choice():
    length = 4
    sample_int = '1234567890'
    result_int = list(random.sample(sample_int, length))
    if len(result_int) == len(set(result_int)):
        result_int = ''.join(result_int)
        return result_int


combination_def = computer_choice()
# print(combination_def)
combination_1 = ''

for i in range(1, 1000):
    try:
        combination_1 = input("Попробуйте отгадать число: ")
        combination_int = int(combination_1)
        combination_dist = set(combination_1)
        if len(combination_dist) == len(combination_def) \
                and type(combination_int) is int:
            if combination_1 == combination_def:
                print('Вы выиграли!')
                break
            else:
                cow = 0
                bull = 0
                for k, v in enumerate(combination_def):
                    if v in combination_1:
                        if combination_1.index(v) == k:
                            bull += 1
                        else:
                            cow += 1
                for k, v in cows_sms.items():
                    for x, y in bulls_sms.items():
                        if k == cow and x == bull:
                            print(v, y)
        else:
            print('Введите комбинацию из четырех неповторяющихся цифр')
    except ValueError:
        print('Введите комбинацию из четырех неповторяющихся цифр')
