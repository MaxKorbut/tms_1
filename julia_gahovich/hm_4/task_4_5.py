"""Есть 2 словаря
a = { 'a': 1, 'b': 2, 'c': 3}
b = { 'c': 3, 'd': 4,'e': 5}

Необходимо их объединить по ключам, а значения ключей поместить в список,
если у одного словаря есть ключ "а", а у другого нету,
то поставить значение None на соответствующую позицию
(1-я позиция для 1-ого словаря, вторая для 2-ого)
ab = {'a':[1, None],'b':[2, None],'c':[3, 3],'d':[None, 4],'e':[None, 5]}
"""

a = dict({'a': 1, 'b': 2, 'c': 3})
b = dict({'c': 3, 'd': 4, 'e': 5})


def task_4_5(a, b):
    c = set(list(a.keys()) + list(b.keys()))
    ab = {}
    for k in c:
        v1 = a.get(k)
        v2 = b.get(k)
        if v1 is None:
            v = [None, v2]
        elif v2 is None:
            v = [v1, None]
        else:
            v = [v1, v2]
        ab[k] = v
    return ab


print(task_4_5(a, b))
