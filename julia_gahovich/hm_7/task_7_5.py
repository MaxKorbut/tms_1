"""Шифр цезаря
Шифр Цезаря — один из древнейших шифров. При шифровании каждый символ
заменяется другим,отстоящим от него в алфавите на фиксированное число позиций.
Примеры:
hello world! -> khoor zruog!
this is a test string -> ymnx nx f yjxy xywnsl

Напишите две функции - encode и decode принимающие как параметр строку и
число - сдвиг.

Бонусные очки
1)Шифр работает на нескольких языках (русский, английский, etc.)
2)Шифр сохраняет исходный регистр и знаки препинания
3)Можно ли обойтись одной функцией для зашифровки и дешифровки?
"""

lower_rus = ''.join(map(chr, range(ord('а'), ord('я') + 1)))
upper_rus = ''.join(map(chr, range(ord('А'), ord('Я') + 1)))
lower_eng = ''.join(map(chr, range(ord('a'), ord('z') + 1)))
upper_eng = ''.join(map(chr, range(ord('A'), ord('Z') + 1)))


def encryption(shift):
    return lower_rus[shift:] + \
        lower_rus[:shift] + \
        upper_rus[shift:] + \
        upper_rus[:shift] + \
        lower_eng[shift:] + \
        lower_eng[:shift] + \
        upper_eng[shift:] + \
        upper_eng[:shift]


def coding(mode='decode', shift=5):
    rus1 = lower_rus + upper_rus + lower_eng + upper_eng
    rus2 = encryption(shift)

    c = {'encode': str.maketrans(rus1, rus2),
         'decode': str.maketrans(rus2, rus1)}

    return c[mode]


print('Юля, \n\nHello!'.translate(coding('encode')))
print('Грд, Mjqqt!'.translate(coding('decode')))
