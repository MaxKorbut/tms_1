"""Напишите функцию декоратор, которая будет измерять время работы
декорируемой функции
from time import time, sleep
time() - показывает количество секунд с 01.01.1970
sleep(N) - приостанавливает выполнение программы на N сек
"""

from time import time, sleep


def decor_time(func):
    def wrapper():
        start_watch = time()
        func()
        print(f'Время работы функции {time() - start_watch} секунд')
        return func
    return wrapper


@decor_time
def function_for_track():
    sleep(3)
    print('Происходит замер времени')
    sleep(3)


function_for_track()
