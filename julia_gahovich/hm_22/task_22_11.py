"""Используя сервис https://fakerestapi.azurewebsites.net и requests
библиотеку, написать запросы:
GET:
Получение списка авторов
Получение конкретного автора по его id
POST:
Добавить новую книгу
Добавить нового пользователя
PUT:
Обновить данные для книги под номером 10
DELETE:
Удалить пользователя под номером 4
"""

import requests


def get_authors_list():
    authors_list_url = 'https://fakerestapi.azurewebsites.net/api/v1/Authors'
    response = requests.get(authors_list_url)
    json_response = response.json()
    for item in json_response:
        print(f"{item['firstName']} {item['lastName']}")


def get_author_by_id(id):
    id_url = f'https://fakerestapi.azurewebsites.net/api/v1/Authors/{id}'
    response = requests.get(id_url)
    json_response = response.json()
    print(f"Автор {json_response['firstName']} {json_response['lastName']}")


def add_new_book():
    add_book_url = 'https://fakerestapi.azurewebsites.net/api/v1/Books'
    json = {
        "id": 666,
        "title": "Курс по питону",
        "description": "О том, как мы страдали",
        "pageCount": 123,
        "excerpt": "Новые знания",
        "publishDate": "2021-02-11T19:19:41.806Z"
    }
    response = requests.post(add_book_url, json=json)
    json_response = response.json()
    print(json_response)


def add_new_user():
    add_user_url = 'https://fakerestapi.azurewebsites.net/api/v1/Users'
    json = {
        "id": 999,
        "userName": "Julia",
        "password": "Gahovich"
    }
    response = requests.post(add_user_url, json=json)
    json_response = response.json()
    print(json_response)


def book_update(id):
    upd_book_url = f'https://fakerestapi.azurewebsites.net/api/v1/Books/{id}'
    json = {
        "id": 666,
        "title": "Курс по питону",
        "description": "Шутка, не страдали",
        "pageCount": 321,
        "excerpt": "Много новых знаний",
        "publishDate": "2021-02-11T19:35:08.166Z"
    }
    response = requests.put(upd_book_url, json=json)
    json_response = response.json()
    print(json_response)


def delete_user(id):
    dlt_user_url = f'https://fakerestapi.azurewebsites.net/api/v1/Users/{id}'
    response = requests.delete(dlt_user_url)
    print(response.status_code)


get_authors_list()
get_author_by_id(1)
add_new_book()
add_new_user()
book_update(10)
delete_user(4)
