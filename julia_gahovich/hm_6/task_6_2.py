"""Подсчет количества букв. На вход подается строка, например, "cccbba"
результат работы программы - строка “c3b2a"

Примеры для проверки работоспособности:
"cccbba" == "c3b2a"
"abeehhhhhccced" == "abe2h5c3ed"
"aaabbceedd" == "a3b2ce2d2"
"abcde" == "abcde"
"aaabbdefffff" == "a3b2def5"
"""

given_string = 'abeehhhhhccced'
cnt = 1
x = 1
compare_string = given_string[x:x + 1]
result_string = ''

for i in given_string:
    if i in compare_string:
        cnt += 1
    else:
        result_string += i
        if cnt != 1:
            result_string += str(cnt)
        cnt = 1
    x += 1
    compare_string = given_string[x:x + 1]

print(result_string)
