from pages.base_page import BasePage
from locator.cart_page_locator import CartPageLocator


class CartPage(BasePage):

    def show_cart_page(self):
        cart_text = self.find_element(
            CartPageLocator.LOCATOR_SHOPPING_CART).text
        check_text = 'SHOPPING-CART SUMMARY'
        assert cart_text == check_text

    def empty_cart(self):
        empty_cart_text = self.find_element(
            CartPageLocator.LOCATOR_EMPTY_CART).text
        check_text = 'Your shopping cart is empty.'
        assert empty_cart_text == check_text

    def product_in_the_cart(self):
        product_check_t_shirt = self.find_element(
            CartPageLocator.LOCATOR_ADDED_PRODUCT).text
        check_text = 'Faded Short Sleeve T-shirts'
        assert product_check_t_shirt == check_text
