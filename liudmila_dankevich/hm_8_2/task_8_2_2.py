# Напишите функцию декоратор, которая будет измерять время работы
# декорируемой функции

# from time import time, sleep

# time() - показывает количество секунд с 01.01.1970
# sleep(N) - приостанавливает выполнение программы на N сек

from time import time, sleep


def decorator(func):

    def wrapper(text):
        start = time()
        func(text)
        sleep(5)
        end = time()
        print(f"Время работы - {end-start} секунд")
    return wrapper


@decorator
def text_new(text):
    print(text.title())


text_new('жыве беларусь! жыве вечна!')
