# игра быки и коровы


import random


# создать список всех возможных ответов

def get_oll_answers():
    ans = []
    for i in range(10000):
        tmp = str(i).zfill(4)
        if len(set(map(int, tmp))) == 4:
            ans.append(list(map(int, tmp)))
    return ans


# выбираем один ответ из списка
def get_one_answer(ans):
    num = random.choice(ans)
    return num


# запрашиваем у пользователя неповторяющиеся цифры
def imput_namber():
    while True:
        nums = input('Введите 4 неповторяющиеся цифры:')
        if len(nums) != 4 or not nums.isdigit():
            continue
        nums = list(map(int, nums))

        if len(set(nums)) == 4:
            break
    return nums


# сравниваем два числа и сообщаем колличество быков и коров
def check(nums, tru_nums):
    bulls, cows = 0, 0
    for i, num in enumerate(nums):
        if num in tru_nums:
            if nums[i] == tru_nums[i]:
                bulls += 1
            else:
                cows += 1
    return bulls, cows


# удали неподходящий вариант из списка возможных
def del_bed_answer(ans, try_nums, bulls, cows):
    for num in ans[:]:
        temp_bull, temp_cow = check(num, try_nums)
        if temp_bull != bulls or temp_cow != cows:
            ans.remove(num)
    return ans


print("игра быки и коровы")
answers = get_oll_answers()
player = imput_namber()
enemy = get_one_answer(answers)

# угадывание игроком числа загадонного компьютером
while True:
    print('=' * 15, 'ход игрока', '=' * 15)
    print('Угадайте число компьютера')
    number = imput_namber()
    bull, cow = check(number, enemy)
    print('быки:', bull, 'коровы:', cow)
    if bull == 4:
        print('Победил игрок')
        print('Компьютер загадал число: ', enemy)
        break

    print('=' * 15, 'ход компьютера', '=' * 15)
    enemy_try = get_one_answer(answers)
    number = imput_namber()
    bull, cow = check(enemy_try, player)
    print('быки:', bull, 'коровы:', cow)
    if bull == 4:
        print('Победил компьютер')
        print('Компьютер загадал число: ', enemy)
        break
    else:
        answers = del_bed_answer(answers, enemy_try, bull, cow)
