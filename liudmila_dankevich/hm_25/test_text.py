# b.	Frames
# i.	Открыть iFrame
# ii.	Проверить, что текст внутри параграфа равен
# “Your content goes here.”

from selenium.webdriver.common.by import By


def test_check_content(browser):
    browser.get('http://the-internet.herokuapp.com/frames')

    button_iframe = '//a[@href="/iframe"]'
    browser.find_element(By.XPATH, button_iframe).click()

    frame = browser.find_element(
        By.XPATH, '//iframe[@id="mce_0_ifr"]')
    browser.switch_to.frame(frame)

    frame_text = browser.find_element(
        By.XPATH, '//body[@class="mce-content-body "]//p')
    assert frame_text.text == "Your content goes here."
