# Лексикографическое возрастание
# На вход подаётся некоторое количество (не больше сотни)
# разделённых пробелом целых чисел
# (каждое не меньше 0 и не больше 19).
# Выведите их через пробел в порядке лексикографического возрастания
# названий этих чисел в английском языке.
# Т.е., скажем числа 1, 2, 3 должны быть выведены в порядке 1, 3, 2,
# поскольку слово two в словаре
# встречается позже слова three, а слово three -- позже слова one
# (иначе говоря, поскольку выражение 'one' <
# 'three' < 'two' является истинным)
# number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
# 5: 'five', 6: 'six', 7: 'seven', 8: 'eight',
# 9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve',13: 'thirteen', 14:
# 'fourteen', 15: 'fifteen',
# 16: 'sixteen', 17: 'seventeen',  18: 'eighteen', 19: 'nineteen'}
def decorator(func):
    def wrapper(str_0):
        number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three',
                        4: 'four', 5: 'five', 6: 'six', 7: 'seven',
                        8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
                        12: 'twelve', 13: 'thirteen',
                        14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
                        17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}

        str_1 = [int(i) for i in str_0.split()]

        dict_1 = {v: k for (k, v) in number_names.items() if k in str_1}

        sorted_k = sorted(dict_1)

        list_1 = [str((dict_1[key])) for key in sorted_k]
        string_1 = ', '.join(list_1)
        return func(string_1)

    return wrapper


@decorator
def sort_1(str_0):
    print(str_0)


sort_1('1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 ')
