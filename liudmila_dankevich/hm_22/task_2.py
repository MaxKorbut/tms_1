# 2)	работа с xml файлом
# Разработайте поиск книги в библиотеке по ее автору(часть имени)
# /цене/заголовку/описанию.

# Файл: library.xml


import xml.etree.ElementTree as Et


def parseXML(root, key, value):
    for child in root:
        for book in child:
            if book.tag == key and value in book.text:
                print(child.tag, child.attrib)


if __name__ == "__main__":
    tree = Et.parse('library.xml')
    root = tree.getroot()


parseXML(root, 'author', 'Matthew')
parseXML(root, 'price', '5.95')
parseXML(root, 'title', 'Midnight Rain')
parseXML(root, 'description', 'A former architect battles')
