# работа с yaml файлом
# * Напечатайте номер заказа
# * Напечатайте адрес отправки
# * Напечатайте описание посылки, ее стоимость и кол-во
# * Сконвертируйте yaml файл в json
# * Создайте свой yaml файл
import json

import yaml

with open('order.yaml') as f:
    order_file = yaml.safe_load(f)
print(type(order_file))


def order_number(value):
    order_num = value['invoice']
    print(f"Заказ №{order_num}")


def address(value):
    addr_1 = value['bill-to']['address']['city']
    addr_2 = value['bill-to']['address']['state']
    print(f"Адрес отправки:города {addr_1}, область {addr_2}")


def info(value):
    for i in range(len(value['product'])):
        description = value['product'][i]['description']
        price = value['product'][i]['price']
        quantity = value['product'][i]['quantity']
        print(f"товар {description}-{quantity} цена {price} за 1 единицу")


def json_fil_1(value):
    with open('convert.json', 'w') as json_file:
        json.dump(str(value), json_file)


if __name__ == "__main__":
    order_number(order_file)
    address(order_file)
    info(order_file)
    json_fil_1(order_file)
