import unittest
from unittest.mock import patch
from homework_12.task_6_2 import counter_symbols


class TestCounterSymbols(unittest.TestCase):

    def setUp(self):
        self.a = ['c', 4, 'b', 3, 'a', 1]
        self.b = ['a', 1]
        self.c = ['1', 1]
        self.d = [' ', 1]

    def tearDown(self):
        pass

    @patch('homework_12.task_6_2.input_line', return_value='abbbcccc')
    def test_validate_and_counter_1(self, input_line):
        self.assertEqual(counter_symbols(input_line()), self.a)

    @patch('homework_12.task_6_2.input_line', return_value='a')
    def test_validate_and_counter_2(self, input_line):
        self.assertEqual(counter_symbols(input_line()), self.b)

    @patch('homework_12.task_6_2.input_line', return_value='1')
    def test_validate_and_counter_3(self, input_line):
        self.assertEqual(counter_symbols(input_line()), self.c)

    @patch('homework_12.task_6_2.input_line', return_value=' ')
    def test_validate_and_counter_4(self, input_line):
        self.assertEqual(counter_symbols(input_line()), self.d)


if __name__ == '__main__':
    unittest.main()
