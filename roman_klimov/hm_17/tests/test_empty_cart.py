from pages.login_page import LoginPage
from pages.cart_page import CartPage
from pages.main_page import MainPage


# Проверка пустой корзины
def test_empty_cart(browser):
    main_page = MainPage(browser)
    main_page.open_base_page()
    main_page.open_login_page()
    login_page = LoginPage(browser)
    login_page.login('test_for_ex@gmail.com', '11111')
    account_page = CartPage(browser)
    account_page.should_be_account_page()
