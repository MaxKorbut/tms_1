from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


def test_field_message(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")
    message = 'hello'

    browser.find_element(
        By.XPATH, '//div//p/textarea[@id="et_pb_contact_message_0"]'
    ).send_keys(message)

    button = browser.find_element(
        By.XPATH, '//div[@id="et_pb_contact_form_0"]//button')

    button.click()
    error_text = browser.find_element(By.XPATH,
                                      '//div[@class="et-pb-contact-message"]/p'
                                      ).text
    assert error_text == "Please, fill in the following fields:"


def test_field_name_and_message(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")

    name = 'Nastya'
    message = 'hello'
    browser.find_element(
        By.XPATH, "//div//p/input[@id='et_pb_contact_name_0']").send_keys(name)

    browser.find_element(
        By.XPATH, '//div//p/textarea[@id="et_pb_contact_message_0"]'
    ).send_keys(message)

    button = browser.find_element(
        By.XPATH, '//div[@id="et_pb_contact_form_0"]//button')
    button.click()

    wait = WebDriverWait(browser, 10)
    wait.until(EC.text_to_be_present_in_element((
        By.CSS_SELECTOR, '[class="et-pb-contact-message"]'),
        'Form filled out successfully'))

    error_text1 = browser.find_element(
        By.CSS_SELECTOR, '[class="et-pb-contact-message"]').text
    assert error_text1 == "Form filled out successfully"


def test_name_field(browser):
    browser.get("https://ultimateqa.com/filling-out-forms/")

    name = 'Nastya'
    browser.find_element(
        By.XPATH, "//div//p/input[@id='et_pb_contact_name_0']").send_keys(name)

    button = browser.find_element(
        By.XPATH, '//div[@id="et_pb_contact_form_0"]//button')
    button.click()

    wait = WebDriverWait(browser, 10)
    wait.until(EC.text_to_be_present_in_element((
        By.CSS_SELECTOR, '[class="et-pb-contact-message"]'),
        'Please, fill in the following fields:'))

    error_text1 = browser.find_element(
        By.XPATH, "//div[@class='et-pb-contact-message']/p").text

    assert error_text1 == 'Please, fill in the following fields:'
