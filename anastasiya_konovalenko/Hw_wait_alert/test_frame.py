import pytest
from selenium.webdriver.common.by import By
from selenium import webdriver


@pytest.fixture()
def browser():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


def test_iframe(browser):
    browser.get("http://the-internet.herokuapp.com/frames")
    browser.find_element(By.XPATH, '//a[@href="/iframe"]').click()
    browser.switch_to.frame(browser.find_element(By.TAG_NAME, "iframe"))
    text_frame = browser.find_element(By.TAG_NAME, "p").text
    assert text_frame == "Your content goes here."
