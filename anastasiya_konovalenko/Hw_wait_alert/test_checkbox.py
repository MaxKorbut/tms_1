from selenium import webdriver
import pytest
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


@pytest.fixture()
def browser():
    driver = webdriver.Chrome()
    yield driver
    driver.quit()


def test_check(browser):
    browser.get("http://the-internet.herokuapp.com/dynamic_controls")

    checkbox = browser.find_element(By.CSS_SELECTOR,
                                    '[type="checkbox"]').click()
    browser.find_element(By.CSS_SELECTOR, '[onclick="swapCheckbox()"]').click()

    wait = WebDriverWait(browser, 5)
    wait.until(EC.visibility_of_element_located((By.ID, "message")))
    find_message = browser.find_element(By.ID, "message").text
    assert find_message == "It's gone!"
    assert checkbox is None

    find_input = browser.find_element(By.CSS_SELECTOR, "[type='text']")
    assert find_input.get_property('disabled') is True

    browser.find_element(By.CSS_SELECTOR, '[onclick="swapInput()"]').click()
    wait.until(EC.visibility_of_element_located((By.ID, "message")))
    find_message_enabled = browser.find_element(By.ID, "message").text
    assert find_message_enabled == "It's enabled!"

    find_input_again = browser.find_element(By.CSS_SELECTOR, "[type='text']")
    assert find_input_again.get_property('disabled') is False
