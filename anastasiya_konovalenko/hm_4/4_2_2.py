import string

lc = string.ascii_lowercase
lu = string.ascii_uppercase
dig = string.digits


def isvalid(password):
    has_no = set(password).isdisjoint
    assert len(password) >= 10, 'password len must be more than 10 characters'
    assert not has_no(dig), 'Password must contain numbers'
    assert not has_no(lc) and not has_no(lu), 'password must contain ' \
                                              'upper/lower case'
    return password


print(isvalid('yhiiuhvuTTGwev42'))
