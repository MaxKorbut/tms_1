from itertools import chain
ab = [
    {'a': 1, 'b': 2, 'c': 3},
    {'c': 3, 'd': 4, 'e': 5}
]
lst = {
    k: [d.get(k) for d in ab]
    for k in chain.from_iterable(ab)
}
print(lst)
