from selenium.webdriver.common.by import By


class ContactPageLocator:
    LOCATOR_CONTACT_TEXT = (By.CLASS_NAME, 'page-subheading')
    LOCATOR_BUTTON_CONTACT_CLICK = (By.ID, 'contact-link')
    LOCATOR_SUBJECT_HEADING = (By.TAG_NAME, "select")
    LOCATOR_EMAIL_FIELD = (By.ID, 'email')
    LOCATOR_ORDER_REFERENCE_FORM = (By.ID, 'id_order')
    LOCATOR_MESSAGE_FORM = (By.ID, 'message')
    LOCATOR_BUTTON_SUBMIT = (By.ID, 'submitMessage')
    LOCATOR_SUCCESS_SENT = (By.CSS_SELECTOR, '[class="alert alert-success"]')
