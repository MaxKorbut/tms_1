import xml.etree.ElementTree as ET

tree = ET.parse("library.xml")
root = tree.getroot()


def find_xml_file(roots, key, value):
    for child in roots:
        for book in child:
            if book.tag == key and value in book.text:
                print(child.tag, child.attrib)


find_xml_file(root, 'price', '17.89')
find_xml_file(root, 'title', 'Learn Python 3 the Hard Way')
find_xml_file(root, 'genre', 'Computer')
